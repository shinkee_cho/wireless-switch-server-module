#define DEBUG_OUT
// https://github.com/esp8266/Arduino/tree/master/libraries/ESP8266WiFi
#include <ESP8266WiFi.h>
// https://techtutorialsx.com/2016/11/20/esp8266-webserver-resolving-an-address-with-mdns/
// https://github.com/esp8266/Arduino/blob/master/libraries/ESP8266mDNS/ESP8266mDNS.cpp
#include <ESP8266mDNS.h>

// Pins
#define PIN_SW_STATE    13
#define PIN_MANUAL_SW   14
#define PIN_MANUAL_MODE 16

#define IsOn         digitalRead(PIN_SW_STATE)
#define IsManualSWOn digitalRead(PIN_MANUAL_SW)
#define IsManualMode digitalRead(PIN_MANUAL_MODE)

// SOFT AP Info
IPAddress localIP_SFAP(192,168,5,1);
IPAddress gateway_SFAP(192,168,5,1);
IPAddress localIP_RT;
IPAddress gateway_RT;
IPAddress subnet(255,255,255,0);
const char* SOFT_AP_SSID = "WirelessSW1";
const char* SOFT_AP_PW = "12345678";

// Router on Internet Info
const char* INTERNET_AP_SSID = "UNDER_WORLD";
const char* INTERNET_AP_PW = "123456789a";

// mDNS
const char* MDNS_NAME = "wireless_sw";

// Web Server Info
WiFiServer server(80);

// UDP Server Info
WiFiUDP Udp;
unsigned int localUdpPort = 4210;  // local port to listen on
char incomingPacket[255];          // buffer for incoming packets

//-----------------------------------------------------------------------------
// Main routine
//-----------------------------------------------------------------------------

//---------------------------
// Set up
//---------------------------
void setup()
{ 
  Serial.begin(115200);
  Serial.println();
  delay(500);

  Init();

  AccessRouter();
  StartSoftAP();
  StartUDPServer();
  StartWebServer();
}

//---------------------------
// Loop
//---------------------------
void loop()
{
  WaitWebClient();
  WaitUDPPacket();
  ManualSwitch();
}


//-----------------------------------------------------------------------------
// Sub routine
//-----------------------------------------------------------------------------

//---------------------------
// Initialize pin modes
//---------------------------
void Init()
{
  // Switch state
  pinMode(PIN_SW_STATE, OUTPUT);
  digitalWrite(PIN_SW_STATE, LOW);

  // Manual Mode and Switch
  pinMode(PIN_MANUAL_MODE, INPUT);
  pinMode(PIN_MANUAL_SW, INPUT);
}

//---------------------------
// Switch on/off manually
//---------------------------
void ManualSwitch()
{
  if(IsManualMode)
  {
    if(IsManualSWOn)
    {
      TurnOn();
    }
    else
    {
      TurnOff();
    }  
  }
}

//---------------------------
// Switch on
//---------------------------
void TurnOn(){ digitalWrite(PIN_SW_STATE, HIGH); }

//---------------------------
// Switch off
//---------------------------
void TurnOff(){ digitalWrite(PIN_SW_STATE, LOW); }

//---------------------------
// Access Router
//---------------------------
void AccessRouter()
{ 
  Serial.println();
  Serial.println("[Access Router]"); 
  
  Serial.printf("Connecting to %s ", INTERNET_AP_SSID);
  
  WiFi.begin(INTERNET_AP_SSID, INTERNET_AP_PW);
  
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }

  Serial.println();
  Serial.print("Connected, Local IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("Connected, Gateway IP address: ");
  Serial.println(WiFi.gatewayIP());


  //---------------------------
  // if Connected
  //---------------------------
  localIP_RT = WiFi.localIP();
  gateway_RT = WiFi.gatewayIP();
  
  StartmDNS();
}

//---------------------------
// Start mDNS service
//---------------------------
void StartmDNS()
{ 
  Serial.println();
  Serial.println("[mDNS]"); 
  
  if (MDNS.begin(MDNS_NAME))
  {
    Serial.printf("mDNS started: %s.local\n", MDNS_NAME);

//-----------------------------
// Failed adding services
//-----------------------------
    //MDNS.addServiceTxt("_esp82xx","_tcp","service", "mDNS");
//    Serial.println("mDNS responder started");
//    MDNS.addService("_esp", "_tcp", 4210); // Announce esp tcp service on port 8080
//  
//    Serial.println("Sending mDNS query");
//    int n = MDNS.queryService("_sub", "_tcp"); // Send out query for esp tcp services
//    Serial.println("mDNS query done");
//    if (n == 0) {
//      Serial.println("no services found");
//    } else {
//      Serial.print(n);
//      Serial.println(" service(s) found");
//      for (int i = 0; i < n; ++i) {
//        // Print details for each service found
//        Serial.print(i + 1);
//        Serial.print(": ");
//        Serial.print(MDNS.hostname(i));
//        Serial.print(" (");
//        Serial.print(MDNS.IP(i));
//        Serial.print(":");
//        Serial.print(MDNS.port(i));
//        Serial.println(")");
//      }
//    }
  }
  else
  {
    Serial.printf("mDNS failed");
  }
}

//---------------------------
// Start soft AP
//---------------------------
void StartSoftAP()
{ 
  Serial.println();
  Serial.println("[Soft AP]");

  //---------------------------
  // Soft AP Configuration
  //---------------------------
  Serial.print("Setting soft-AP configuration ... ");
  Serial.println(WiFi.softAPConfig(localIP_SFAP, gateway_SFAP, subnet) ? "Ready" : "Failed!");

  Serial.print("Setting soft-AP ... ");

  //---------------------------
  // Run Soft AP
  //---------------------------
  if(WiFi.softAP(SOFT_AP_SSID, SOFT_AP_PW))
  {
    Serial.println("Ready");

    // pin 13 Light on
    pinMode(13, OUTPUT);
    digitalWrite(13, HIGH);
  }
  else
  {
    Serial.println("Failed!");
  }

  Serial.print("Soft-AP Local IP address   : ");
  Serial.println(WiFi.softAPIP());
  Serial.print("Soft-AP Gateway IP address : ");
  Serial.println(gateway_SFAP);

  //---------------------------
  // Set to ips
  //---------------------------
  localIP_SFAP = WiFi.softAPIP();
}

//---------------------------
// Starts Web server
// Any devices can be reached and control the switch via web browsers.
//---------------------------
void StartWebServer()
{ 
  Serial.println();
  Serial.println("[WEB Server]"); 
  
  server.begin();
  Serial.println("Web server started");
  Serial.printf("Soft AP: Open %s in a web browser\n", localIP_SFAP.toString().c_str());
  Serial.printf("Router : Open %s in a web browser\n", localIP_RT.toString().c_str());
}

//---------------------------
// Start UDP Server
//---------------------------
void StartUDPServer()
{ 
  Serial.println();
  Serial.println("[UDP Server]"); 
  
  Udp.begin(localUdpPort);
  Serial.printf("Soft AP: Now listening at IP %s, UDP port %d\n", localIP_SFAP.toString().c_str(), localUdpPort);
  Serial.printf("Router : Now listening at IP %s, UDP port %d\n", localIP_RT.toString().c_str(), localUdpPort);
}


//---------------------------
// Waits udp packets
//---------------------------
void WaitUDPPacket()
{ 
  int packetSize = Udp.parsePacket();
  if (packetSize)
  {
    // receive incoming UDP packets
    Serial.printf("Received %d bytes from %s, port %d\n", packetSize, Udp.remoteIP().toString().c_str(), Udp.remotePort());

    //---------------------------
    // Check the packet
    //---------------------------
    String state = "off";
    int len = Udp.read(incomingPacket, 255);
    
    if (len > 0)
    {
      incomingPacket[len] = 0;
      state = incomingPacket;
    }

    Serial.printf("UDP packet contents: %s\n", incomingPacket);

    //---------------------------
    // Turn On and Off
    //---------------------------
    if(state == "on")
    {
      TurnOn();
      Serial.println("Turned on");
    }
    else if(state == "off")
    {
      TurnOff();
      Serial.println("Turned off");
    }
    else if(state =="state")
    {
      Serial.println("SW State Required");
    }

    //---------------------------
    // Send back a reply
    //---------------------------
    // send back a reply, to the IP address and port we got the packet from
    Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
    
    if(IsOn){ Udp.write("on"); }
    else{ Udp.write("off"); }
    
    Udp.endPacket();  // send
  }
}


//---------------------------
// Waits web clients
//---------------------------
void WaitWebClient()
{  
  
  WiFiClient client = server.available();
  //---------------------------
  // wait for a client (web browser) to connect
  //---------------------------
  if (client)
  {
    Serial.println("\n[Client connected]");
    
    int cntToBreak = 0;
    String strHeader = "";
    
    while (client.connected())
    {
      //---------------------------
      // read line by line what the client (web browser) is requesting
      // available() returns the size of message.
      //---------------------------
      if(client.available())
      {
        String line = client.readStringUntil('\r');
        Serial.print(line);
        client.readStringUntil('\n');

        strHeader += line;
        
        //---------------------------
        // wait for end of client's request, that is marked with an empty line
        //---------------------------
        if(line.length() == 0)
        {
          String page = GetHeader();

          //---------------------------
          // Ajax request with command
          //---------------------------
          if(strHeader.indexOf("SWState") > -1)
          {
            if(IsOn)
            {
              page += "on";
            }
            else
            {
              page += "off";
            }
          }
          else if(strHeader.indexOf("SWOn") > -1)
          {
            TurnOn();
            page += "on";
          }
          else if(strHeader.indexOf("SWOff") > -1)
          {
            TurnOff();
            page += "off";
          }
          //---------------------------
          // Main page Request
          //---------------------------
          else
          {
            page += GetHtml();
          }

          //---------------------------
          // Send a requested page
          //---------------------------
          client.print(page);
          break;
        }
        
        Serial.println();
        //Serial.println(line.length());
      }
      //---------------------------
      // Related to Chrome bug
      // Client is connected but no message.
      // then count the times of this loop.
      //---------------------------
      else
      {
        if(cntToBreak++ > 1000)
        {
          Serial.println("Broke");
          cntToBreak = 0;
          break;
        }
      }
    }
    delay(1); // give the web browser time to receive the data

    //---------------------------
    // close the connection:
    //---------------------------
    if(client.stop())
    {
      Serial.println("[Client disonnected]");
    }
    else
    {
      Serial.println("[Client disonnection failed]");
    }
  }
}


//---------------------------
// Get header of http service
//---------------------------
String GetHeader()
{
  String strHeader =
     String("HTTP/1.1 200 OK\r\n") +
            "Content-Type: text/html\r\n" +
            //"Connection: close\r\n" +  // the connection will be closed after completion of the response
            //"Refresh: 5\r\n" +  // refresh the page automatically every 5 sec
            "Connection: keep-alive\r\n" + 
            "\r\n"; 
  return strHeader;
}

//---------------------------
// Prepare a web page to be send to a client (web browser)
//---------------------------
String GetHtml()
{
  String htmlPage =
     String("<!DOCTYPE HTML>\n") +
            "<html>" +
            
            "<head>" +
            
              "<title>Wireless Switch V.1.0.0</title>" +
              
              "<style>" +
                "button.on, button.off {width: 100px; height:50px; border-radius: 10px; }" +
                "button.switchOn, button.switchOff {width: 200px; height:50px; }" +
                "button.on, button.switchOn { background:orange; }" +
                "button.off, button.switchOff { background:gray; }" +
              "</style>" +
              
              "<script>" + 

               "function AjaxReceive()\n" +
                "{\n" +
                  "if(this.readyState == 4)\n" +
                  "{\n" +
                    "if(this.status == 200)\n" +
                    "{\n" +
                      "if(this.responseText != null)\n" +
                      "{\n" +
                        "var btnState = document.getElementById('BtnState');\n" +
                        "var btnSwitch = document.getElementById('BtnSwitch');\n" +
                        "var strState = this.responseText.toLowerCase();\n" +
                        
                        "if(strState == 'on')\n" +
                        "{\n" +
                          "btnState.innerHTML = 'ON';\n" +
                          "btnState.className = 'on';\n" +
                          "btnSwitch.innerHTML = 'Switch OFF';\n" +
                          "btnSwitch.className = 'switchOff';\n" +
                        "}\n" +
                        "else\n" +
                        "{\n" +
                          "btnState.innerHTML = 'OFF';\n" +
                          "btnState.className = 'off';\n" +
                          "btnSwitch.innerHTML = 'Switch ON';\n" +
                          "btnSwitch.className = 'switchOn'\n;" +
                        "}\n" +
                      "}\n" +
                    "}\n" +
                  "}\n" +
                "}\n" +

                  
                "function GetSwitchState()\n" +
                "{\n" +
                  "nocache = '&nocache=' + Math.random() * 1000000;\n" +
                  "var request = new XMLHttpRequest();\n" +
                  "request.onreadystatechange = AjaxReceive;\n" +
                  "request.open('GET', 'SWState' + nocache, true);\n" +
                  "request.send(null);\n" + 
                  "setTimeout('GetSwitchState()', 500);\n" + 
                "}\n" +

                "function SetSwitchState()\n" +
                "{\n" +
                  "var state = document.getElementById('BtnState').className;\n" +
                  "var cmd = '';\n" +
                  "if(state == 'on')\n" +
                  "{ cmd = 'SWOff'; }\n" +
                  "else\n" +
                  "{ cmd = 'SWOn'; }\n" +
                  
                  "nocache = '&nocache=' + Math.random() * 1000000;\n" +
                  "var request = new XMLHttpRequest();\n" +
                  "request.onreadystatechange = AjaxReceive;\n" +
                  "request.open('GET', cmd + nocache, true);\n" +
                  "request.send(null);\n" + 
                "}\n" +
              "</script>\n" +
            "</head>\n" +
            
            "<body onload='GetSwitchState()'>\n" +
              "<button id='BtnState' class='off'>OFF</button>\n" +
              "<button id='BtnSwitch' class='switchOn' onclick='SetSwitchState()'>Switch ON</button>\n" +
            "</body>\n" +
            
            "</html>\n" +
            "\r\n";
  return htmlPage;
}
