#include <ESP8266WiFi.h>
// https://techtutorialsx.com/2016/11/20/esp8266-webserver-resolving-an-address-with-mdns/
// https://github.com/esp8266/Arduino/blob/master/libraries/ESP8266mDNS/ESP8266mDNS.cpp
#include <ESP8266mDNS.h>

// Pins
#define IsSoftAPSelected digitalRead(PIN_SELECT_AP)
#define IsOnState digitalRead(PIN_ON_OFF)
#define PIN_ROUTER_LED    12
#define PIN_SERVER_AP_LED 13
#define PIN_SELECT_AP     16
#define PIN_ON_OFF        14
bool IsOn = false;

// SOFT AP Info
const char* SOFT_AP_SSID = "WirelessSW1";
const char* SOFT_AP_PW = "12345678";

// Router on Internet Info
const char* INTERNET_AP_SSID = "UNDER_WORLD";
const char* INTERNET_AP_PW = "123456789a";

// UDP Server Info
WiFiUDP Udp;
unsigned int serverUDPPort = 4210;  // Server port
unsigned int localUDPPort = 4211;   // Local port
char incomingPacket[255];  // buffer for incoming packets

IPAddress UDPServerIP;
const char* ip;

// mDNS
#define MDNS_HOSTNAME "sw"

//-----------------------------------------------------------------------------
// Main routine
//-----------------------------------------------------------------------------

//---------------------------
// Set up
//---------------------------
void setup()
{
  // Sets Serial Monitor
  Serial.begin(115200);
  Serial.println();
  delay(500); // wait for serial port to connect. Needed for native USB port only

  Init();

  //---------------------------
  // Selects an AP
  //---------------------------
  // Selects which AP will be accessed by button(io pin 14). 
  if(IsSoftAPSelected)
  {
    AccessSoftAP();
    digitalWrite(PIN_SERVER_AP_LED, HIGH);    
    UDPServerIP = WiFi.gatewayIP();
  }
  else
  {
    AccessRouter();
    digitalWrite(PIN_ROUTER_LED, HIGH);

    MDNS.begin("");
    
    int numAnswers = MDNS.queryService(MDNS_HOSTNAME, "tcp");
    
    for(int i=0; i<numAnswers;i++)
    {
      Serial.printf("Host name %s, index: %d\n", MDNS.hostname(i).c_str(), i);
      Serial.printf("Host ip %s, index: %d\n", MDNS.IP(i).toString().c_str(), i);
      Serial.printf("Host port %d, index: %d\n", MDNS.port(i), i);
    }
    
    //UDPServerIP = MDNS_HOSTNAME;
  }

  //---------------------------
  // Set local port to receive
  //---------------------------
  Udp.begin(localUDPPort);
  
  GetSWState();

  Serial.println(WiFi.hostname());
}

//---------------------------
// Loop
//---------------------------
void loop()
{
  TurnOnOff();
  CheckSWState();
}



//-----------------------------------------------------------------------------
// Sub routine
//-----------------------------------------------------------------------------

//---------------------------
// Initialize pin modes
//---------------------------
void Init()
{
  // Disable AP Mode
  WiFi.mode(WIFI_STA);

  // Button to select an AP
  pinMode(PIN_SELECT_AP, INPUT);

  // Init the leds of AP modes
  pinMode(PIN_ROUTER_LED, OUTPUT);
  digitalWrite(PIN_ROUTER_LED, LOW);
  pinMode(PIN_SERVER_AP_LED, OUTPUT);
  digitalWrite(PIN_SERVER_AP_LED, LOW);
}

//---------------------------
// Get Switch state
//---------------------------
void GetSWState()
{
  int resultBegin = Udp.beginPacket(UDPServerIP, serverUDPPort);
  Udp.write("state");
  int resultEnd = Udp.endPacket();

  Serial.println();
  Serial.println("[Check the state of SW]");
  Serial.println("Get the state of the switch.");
  Serial.printf("Begin packet return: %d \n", resultBegin);
  Serial.printf("End packet return: %d \n", resultEnd);
  Serial.printf("Server ip: %s\n", UDPServerIP.toString().c_str());

  CheckSWState();
}

//---------------------------
// Turn on and off
//---------------------------
void TurnOnOff()
{
  //---------------------------
  // Do it once
  //---------------------------
  if(IsOnState && !IsOn)
  {
    int resultBegin = Udp.beginPacket(UDPServerIP, serverUDPPort);
    Udp.write("on");
    int resultEnd = Udp.endPacket();
    IsOn = true;

    Serial.println();
    Serial.println("[Turn On]");
    Serial.printf("Begin packet return: %d \n", resultBegin);
    Serial.printf("End packet return: %d \n", resultEnd);
    Serial.printf("Server ip: %s\n", UDPServerIP.toString().c_str());
  }
  else if(!IsOnState && IsOn)
  {
    int resultBegin = Udp.beginPacket(UDPServerIP, serverUDPPort);
    Udp.write("off");
    int resultEnd = Udp.endPacket();
    IsOn = false;

    Serial.println();
    Serial.println("[Turn Off]");
    Serial.printf("Begin packet return: %d \n", resultBegin);
    Serial.printf("End packet return: %d \n", resultEnd);
    Serial.printf("Server ip: %s\n", UDPServerIP.toString().c_str());
  }
}


//---------------------------
// Check switch state from server
//---------------------------
bool CheckSWState()
{
  int packetSize = Udp.parsePacket();
  
  if (Udp.available())
  {
    String state;
    int len = Udp.read(incomingPacket, 255);

    //---------------------------
    // Check the received packet
    //---------------------------
    if (len > 0)
    {
      incomingPacket[len] = 0;
      state = incomingPacket;
    }

    //---------------------------
    // Set state
    //---------------------------
    if(state == "on"){ IsOn = true; Serial.println("on------");}
    else { IsOn = false; }

    // receive incoming UDP packets
    Serial.println();
    Serial.println("[Received message]");
    Serial.printf("Received %d bytes from %s, port %d\n", packetSize, UDPServerIP.toString().c_str(), serverUDPPort);
    Serial.printf("UDP packet contents: %s\n", incomingPacket);
  }
}


//---------------------------
// Access Soft AP
//---------------------------
void AccessSoftAP()
{
  Serial.println();
  Serial.println("[Accessed Soft AP]");
  
  Serial.printf("Connecting to %s ", SOFT_AP_SSID);
  
  WiFi.begin(SOFT_AP_SSID, SOFT_AP_PW);
  //WiFi.config(local_IP, gateway, subnet);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  
  Serial.println();
  Serial.println("Connected");
  
  Serial.print("Local IP address: ");
  Serial.println(WiFi.localIP());
  
  Serial.print("Gateway IP address: ");
  Serial.println(WiFi.gatewayIP().toString().c_str());
}

//---------------------------
// Access Router
//---------------------------
void AccessRouter()
{ 
  Serial.println();
  Serial.println("[Accessed Router]");
  
  Serial.printf("Connecting to %s ", INTERNET_AP_SSID);
  
  WiFi.begin(INTERNET_AP_SSID, INTERNET_AP_PW);
  
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  
  Serial.println();
  Serial.println("Connected");
  
  Serial.print("Local IP address: ");
  Serial.println(WiFi.localIP());
  
  Serial.print("Gateway IP address: ");
  Serial.println(WiFi.gatewayIP().toString().c_str());
}
